package com.example.app.ui.splash

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.app.ui.PREFERENCES_IS_USER_LOGGED

class SplashViewModel : ViewModel() {

    private lateinit var sharedPreferences: SharedPreferences

    private val _userLogged = MutableLiveData<Boolean>()
    val userLogged: LiveData<Boolean> = _userLogged

    fun setSharedPreferences(preferences: SharedPreferences) {
        sharedPreferences = preferences
    }

    fun checkUser() {
        _userLogged.value = if (this::sharedPreferences.isInitialized) sharedPreferences.getBoolean(
            PREFERENCES_IS_USER_LOGGED,
            false
        )
        else false
    }
}