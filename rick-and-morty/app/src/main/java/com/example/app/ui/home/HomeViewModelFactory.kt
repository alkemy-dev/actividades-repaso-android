package com.example.app.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.app.data.character.CharacterRemoteDataSource
import com.example.app.data.character.CharacterRepository

class HomeViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val remoteDataSource = CharacterRemoteDataSource()
        val repository = CharacterRepository(remoteDataSource)

        return HomeViewModel(repository) as T
    }
}