package com.example.app.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.app.data.NetworkResponse
import com.example.app.data.NetworkStatus
import com.example.app.data.Resource
import com.example.app.data.character.CharacterRepository
import com.example.app.ui.model.Character

class HomeViewModel constructor(
    private val repository: CharacterRepository
) : ViewModel() {

    private val _characterList = MutableLiveData<Resource<List<Character>>>()
    val characterList: LiveData<Resource<List<Character>>> = _characterList

    init {
        getCharacterList()
    }

    private fun getCharacterList() {
        _characterList.value = Resource(NetworkStatus.LOADING)
        val response = object : NetworkResponse<List<Character>> {
            override fun onResponse(value: Resource<List<Character>>) {
                _characterList.value = value
            }
        }
        repository.getCharacterList(response)
    }
}