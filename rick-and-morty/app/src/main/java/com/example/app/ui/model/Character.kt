package com.example.app.ui.model

import com.google.gson.annotations.SerializedName

data class Character(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("image") val image: String
)

data class CharacterListResponse(
    @SerializedName("results") val results: List<Character>
)