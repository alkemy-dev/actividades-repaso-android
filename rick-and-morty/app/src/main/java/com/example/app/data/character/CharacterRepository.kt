package com.example.app.data.character

import com.example.app.data.NetworkResponse
import com.example.app.ui.model.Character

class CharacterRepository constructor(
    private val remoteDataSource: CharacterRemoteDataSource
) {
    fun getCharacterList(networkResponse: NetworkResponse<List<Character>>) {
        return remoteDataSource.getCharacterList(networkResponse)
    }
}