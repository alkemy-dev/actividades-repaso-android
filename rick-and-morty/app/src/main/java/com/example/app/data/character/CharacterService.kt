package com.example.app.data.character

import com.example.app.ui.model.CharacterListResponse
import retrofit2.Call
import retrofit2.http.GET

interface CharacterService {

    @GET("/api/character")
    fun getCharacterList(): Call<CharacterListResponse>
}