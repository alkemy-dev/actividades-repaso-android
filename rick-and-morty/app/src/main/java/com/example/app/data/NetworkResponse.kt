package com.example.app.data

interface NetworkResponse<T> {
    fun onResponse(value: Resource<T>)
}