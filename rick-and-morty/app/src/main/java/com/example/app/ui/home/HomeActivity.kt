package com.example.app.ui.home

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.example.app.R
import com.example.app.databinding.ActivityHomeBinding
import com.example.app.data.NetworkStatus

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding
    private val viewModel by viewModels<HomeViewModel>(factoryProducer = { HomeViewModelFactory() })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setObservers()
    }

    private fun setObservers() {
        viewModel.characterList.observe(this, Observer {

            binding.progressBar.isVisible = it.status == NetworkStatus.LOADING

            when (it.status) {
                NetworkStatus.SUCCESS -> {
                    val data =
                        it.data?.map { item -> HomeItem(item.name, item.image) } ?: emptyList()
                    binding.characters.adapter = HomeAdapter(data)
                }
                NetworkStatus.ERROR -> Toast.makeText(this, R.string.home_error, Toast.LENGTH_LONG).show()
                else -> {
                }
            }
        })
    }
}