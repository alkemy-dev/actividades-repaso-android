package com.example.app.ui.login

import android.content.SharedPreferences
import android.util.Patterns
import com.example.app.ui.PREFERENCES_IS_USER_LOGGED

class LoginPresenter {

    private var loginView: LoginView? = null
    private lateinit var sharedPreferences: SharedPreferences

    fun attachView(view: LoginView) {
        loginView = view
    }

    fun detachView() {
        loginView = null
    }

    fun setSharedPreferences(preferences: SharedPreferences) {
        sharedPreferences = preferences
    }

    fun onLoginDataChanged(email: String, password: String) {
        val isValidEmail = isValidEmail(email)
        val isValidPassword = isValidPassword(password)
        loginView?.showErrorEmailInvalid(isValidEmail)
        loginView?.enableButton(isValidEmail && isValidPassword)
    }

    fun doLogin() {
        if (this::sharedPreferences.isInitialized) sharedPreferences.edit().apply {
            putBoolean(PREFERENCES_IS_USER_LOGGED, true)
            apply()
        }
        loginView?.navigateToHome()
    }

    // Email validation check
    private fun isValidEmail(email: String) = Patterns.EMAIL_ADDRESS.matcher(email).matches()

    // Password validation check
    private fun isValidPassword(password: String) = password.length > 5
}

interface LoginView {
    fun showErrorEmailInvalid(isValid: Boolean)
    fun enableButton(isEnabled: Boolean)
    fun navigateToHome()
}