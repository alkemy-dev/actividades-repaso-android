package com.example.app.ui.home

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.app.databinding.HomeItemBinding

class HomeAdapter(private val data: List<String>) : RecyclerView.Adapter<HomeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val image = HomeItemBinding.inflate(inflater, parent, false)
        return HomeViewHolder(image)
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val image = data[position]
        holder.bind(image)
    }

    override fun getItemCount() = data.size
}

class HomeViewHolder(private val binding: HomeItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(name: String) {
        val uri = Uri.parse("android.resource://com.example.app/mipmap/$name")
        with(binding) {
            image.setImageURI(uri)
            description.text = name
        }
    }
}