package com.example.app.ui.splash

import android.content.SharedPreferences
import com.example.app.ui.PREFERENCES_IS_USER_LOGGED

class SplashPresenter {
    private var splashView: SplashView? = null
    private lateinit var sharedPreferences: SharedPreferences

    fun attachView(view: SplashView) {
        splashView = view
    }

    fun detachView() {
        splashView = null
    }

    fun setSharedPreferences(preferences: SharedPreferences) {
        sharedPreferences = preferences
    }

    fun checkUser() {
        if (this::sharedPreferences.isInitialized) {
            if (sharedPreferences.getBoolean(
                    PREFERENCES_IS_USER_LOGGED,
                    false
                )
            ) splashView?.navigateToHome() else splashView?.navigateToLogin()
        }
    }
}

interface SplashView {
    fun navigateToHome()
    fun navigateToLogin()
}