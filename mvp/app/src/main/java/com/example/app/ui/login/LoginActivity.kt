package com.example.app.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import com.example.app.R
import com.example.app.databinding.ActivityLoginBinding
import com.example.app.ui.home.HomeActivity

class LoginActivity : AppCompatActivity(), LoginView {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sharedPreferences = getSharedPreferences(
            getString(R.string.shared_preferences_file_key),
            Context.MODE_PRIVATE
        )

        presenter = LoginPresenter().apply {
            attachView(this@LoginActivity)
            setSharedPreferences(sharedPreferences)
        }

        binding.email.doAfterTextChanged {
            presenter.onLoginDataChanged(
                binding.email.text.toString(),
                binding.password.text.toString()
            )
        }

        binding.password.doAfterTextChanged {
            presenter.onLoginDataChanged(
                binding.email.text.toString(),
                binding.password.text.toString()
            )
        }

        binding.login.setOnClickListener {
            presenter.doLogin()
        }
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun showErrorEmailInvalid(isValid: Boolean) {
        binding.email.error = if (!isValid) getString(R.string.login_invalid_email) else null
    }

    override fun enableButton(isEnabled: Boolean) {
        binding.login.isEnabled = isEnabled
    }

    override fun navigateToHome() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }
}