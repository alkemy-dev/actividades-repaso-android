package com.example.app.ui.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.app.R
import com.example.app.ui.home.HomeActivity
import com.example.app.ui.login.LoginActivity

class SplashActivity : AppCompatActivity() {

    private val splashViewModel by viewModels<SplashViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPreferences = getSharedPreferences(
            getString(R.string.shared_preferences_file_key),
            Context.MODE_PRIVATE
        )
        splashViewModel.setSharedPreferences(sharedPreferences)

        splashViewModel.userLogged.observe(this, Observer { userLogged ->
            val intent = if (userLogged) Intent(this, HomeActivity::class.java)
            else Intent(this, LoginActivity::class.java)

            startActivity(intent)
        })
    }

    override fun onResume() {
        super.onResume()
        splashViewModel.checkUser()
    }
}