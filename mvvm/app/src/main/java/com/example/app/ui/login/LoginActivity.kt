package com.example.app.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import com.example.app.R
import com.example.app.databinding.ActivityLoginBinding
import com.example.app.ui.home.HomeActivity

class LoginActivity : AppCompatActivity() {

    private val loginViewModel by viewModels<LoginViewModel>()
    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sharedPreferences = getSharedPreferences(
            getString(R.string.shared_preferences_file_key),
            Context.MODE_PRIVATE
        )
        loginViewModel.setSharedPreferences(sharedPreferences)

        loginViewModel.dataValid.observe(this, Observer { dataValid ->
            binding.login.isEnabled = dataValid
        })

        loginViewModel.emailValid.observe(this, Observer { emailValid ->
            binding.email.error = if (!emailValid) getString(R.string.login_invalid_email) else null
        })

        binding.email.doAfterTextChanged {
            loginViewModel.loginDataChanged(
                binding.email.text.toString(),
                binding.password.text.toString()
            )
        }

        binding.password.doAfterTextChanged {
            loginViewModel.loginDataChanged(
                binding.email.text.toString(),
                binding.password.text.toString()
            )
        }

        binding.login.setOnClickListener {
            loginViewModel.login()

            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}