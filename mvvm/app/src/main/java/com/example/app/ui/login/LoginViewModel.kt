package com.example.app.ui.login

import android.content.SharedPreferences
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.app.ui.PREFERENCES_IS_USER_LOGGED

class LoginViewModel : ViewModel() {

    private lateinit var sharedPreferences: SharedPreferences

    private val _dataValid = MutableLiveData<Boolean>()
    val dataValid: LiveData<Boolean> = _dataValid

    private val _emailValid = MutableLiveData<Boolean>()
    val emailValid: LiveData<Boolean> = _emailValid

    fun setSharedPreferences(preferences: SharedPreferences) {
        this.sharedPreferences = preferences
    }

    fun loginDataChanged(email: String, password: String) {
        val isEmailValid = isEmailValid(email)
        _emailValid.value = isEmailValid
        _dataValid.value = isEmailValid && isPasswordValid(password)
    }

    fun login() {
        if (this::sharedPreferences.isInitialized) sharedPreferences.edit().apply {
            putBoolean(PREFERENCES_IS_USER_LOGGED, true)
            apply()
        }
    }

    // Email validation check
    private fun isEmailValid(email: String) = Patterns.EMAIL_ADDRESS.matcher(email).matches()

    // Password validation check
    private fun isPasswordValid(password: String) = password.length > 5
}